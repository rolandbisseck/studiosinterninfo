/**
 * Created by Packard bell on 9/17/2015.
 */

public class AlignColumns
{
    public static void main(String[] args)
    {
        String[]text = {"Given$a$text$file$of$many$lines,$where$fields$within$a$line$\n" +
                "are$delineated$by$a$single$'dollar'$character,$write$a$program\n" +
                "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each$\n" +
                "column$are$separated$by$at$least$one$space.\n" +
                "Further,$allow$for$each$word$in$a$column$to$be$either$left$\n" +
                "justified,$right$justified,$or$center$justified$within$its$column"};

            String[] information = null;

            System.out.println("LEFT");
            for(String myText : text)
            {
                information = myText.split("\\$");
                 for(String left : information)
                 {
                    System.out.printf("%-15s", left  );
                 }
           }
                System.out.println("\n");
                System.out.println("============================================================================================================");

        String[] information2 = null;
        System.out.println("RIGHT");
        for(String myText : text)
        {
            information2 = myText.split("\\$");
            for(String right : information2)
            {
                System.out.printf("%15s", right  );
            }
        }
        System.out.println("\n");
        System.out.println("============================================================================================================");

        String[] information3 = null;
        System.out.println("CENTRE");
        for(String myText : text)
        {
            information3 = myText.split("\\$");
            for(String centre : information3)
            {
                System.out.printf("%5s", centre);
            }
        }
        System.out.println("\n");
        System.out.println("============================================================================================================");
    }
}
