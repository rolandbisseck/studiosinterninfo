import java.io.IOException;
import java.util.ArrayList;
import java.io.*;
import java.util.Arrays;
import java.net.*;
import java.util.*;

/**
 * Created by Packard bell on 9/20/2015.
 */
public class Anagrams
{
    public static void main(String[] args) throws IOException
    {
        URL url = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
        InputStreamReader stream = new InputStreamReader(url.openStream());
        BufferedReader reader = new BufferedReader(stream);

        Map<String, Collection<String>> anagrams = new HashMap<String, Collection<String>>();
        String word;
        int count = 0;
        while ((word = reader.readLine()) != null)
        {
            char[] chars = word.toCharArray();
            Arrays.sort(chars);
            String key = new String(chars);
            if (!anagrams.containsKey(key))
            {
                anagrams.put(key, new ArrayList<String>());
                anagrams.get(key).add(word);

            }
            count = Math.max(count, anagrams.get(key).size());
        }

        reader.close();

        for(Collection<String> anagram : anagrams.values())
        {
            if (anagram.size() >= count)
            {
                System.out.println(anagram);
            }

        }

    }
}
