import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Packard bell on 9/23/2015.
 */
public class BalancingBrackets
{
    public static void main(String[] args)
    {
        String results = "";
        String[] tests = {"(empty)", "[]", "[][]","[[][]]", "][" ,"][][", "[]][[]"};
        for (int i = 0; i <= 16; i += 2)
        {
            generateArray(i);

        }

        for (String test : tests)
        {
            if(test.isEmpty() == true )
            {
                results = "OK";
            }
            else if (checkBrackets(test) == true )
            {
                results = "OK";
            }
            else
            {
                results = "NOT OK";
            }
            System.out.println(test + "  " + results);
        }
    }
    public static boolean checkBrackets(String str) {
        int unbalancedBrackets = 0;
        for (char ch : str.toCharArray()) {
            if (ch == '[') {
                unbalancedBrackets++;
            } else if (ch == ']') {
                unbalancedBrackets--;
            }
            if (unbalancedBrackets < 0)
            {
                return false;
            }
        }
        return unbalancedBrackets == 0;
    }

    static String generateArray(int n)
    {
        if (n % 2 == 1) {
            return null;
        }
        String str2 = "";
        int openBracketsLeft = n / 2;
        int unclosed = 0;
        while (str2.length() < n) {
            if (((Math.random() >= 5) && (openBracketsLeft > 0)) || (unclosed == 0))
            {
                str2 += '[';
                openBracketsLeft--;
                unclosed++;
            } else {
                str2 += ']';
                unclosed--;
            }
        }
        return str2;
    }

}