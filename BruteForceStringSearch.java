/**
 * Created by Packard bell on 9/22/2015.
 */
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import javax.swing.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BruteForceStringSearch
{
    public static void main(String[] args)
    {
        String pattern = JOptionPane.showInputDialog(null, "enter the pattern");
        String text = JOptionPane.showInputDialog(null, "enter a text");
        int match = search(pattern, text);
        System.out.println(match);
    }
    /*
        this method will return first location of the pattern if there is the match else it will a -1 to say that there is no match
    */
    public static int search(String pattern, String text)
    {
        int position = 0;
        for(int x = 0; x < text.length() - pattern.length(); x++)
        {
            int y = 0;
            while(y < pattern.length() && text.charAt(x + y) == pattern.charAt(y))
            {
                y++;

            }
            if(y == pattern.length())
            {
                // return the index of the first location if there is the match
                position = x;
            }

        }
        return position;
    }

}
