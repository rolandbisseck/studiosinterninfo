/**
 * Created by Packard bell on 9/22/2015.
 */
public class CallingMethod
{

    public CallingMethod()
    {

    }
    //this method requires no arguments.
    public  void noArguments()
    {
        int number = 12;
        double price = 0.5;
        double sum = 0;
        sum = number * price;

    }
    //this method requires a fixed number of argument.
    public double setFixedNumberOfArguments(int number, double price)
    {
        double total = 0;
        total = number * price;
        return total;
    }
    //this method required an optional argument(receiving one parameter);
    public double  setOptionalArgument(int age)
    {
        double amount = 0;
        amount = age * 10;
        return age;
    }
    //this method required an optional argument(receiving two parameter);
    public void setOptionalArgument(String name, int age)
    {

    }
    //this method required an optional argument(receiving none parameter);
    public void setOptionalArgument()
    {

    }
    //Calling a method with a variable number of arguments,
    public  void setVariableNumberOfArgument(String name, int age, double price)
    {

    }
    //Obtaining the return value of a method,
    public double getCost(int number, double cost)
    {
        double costTotal = 0;
        costTotal = number * cost;
        return costTotal;
    }
    private void privateMethod()
    {

    }
    protected double protectedMethod(int product, double cost)
    {
        double priceTotal = 0.0;
        priceTotal = product* cost;
        return priceTotal;
    }

}
