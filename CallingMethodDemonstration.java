import javax.swing.*;

/**
 * Created by Packard bell on 9/28/2015.
 */
public class CallingMethodDemonstration
{
    public static void main(String[] args)
    {
        //i instiantiate an object of the callingmethod class to access all the method created in that class
        CallingMethod call = new CallingMethod();

        //CALLING METHOD WITH NO ARGUMENT
        call.noArguments();

        //calling a method  that requires a fixed number of argument, passed by value
            double total = call.setFixedNumberOfArguments(Integer.parseInt(JOptionPane.showInputDialog(null, "enter a number")), Double.parseDouble(JOptionPane.showInputDialog(null, "enter a number")));
            System.out.println(total);

        //this method required an optional argument(receiving one parameter);
         double amount = call.setOptionalArgument(Integer.parseInt(JOptionPane.showInputDialog(null, "enter your age")));
        System.out.println(amount);

        //Calling a method with a variable number of arguments, passed by value
        String name = JOptionPane.showInputDialog(null, "enter your name");
        int age = Integer.parseInt(JOptionPane.showInputDialog(null, "enter your age"));
        double price = Double.parseDouble(JOptionPane.showInputDialog(null, "enter your price"));
        call.setVariableNumberOfArgument(name,age,price);
        System.out.println(name+"," + age + "," +price);

        //calling a method that return value, passed by value
        int number = Integer.parseInt(JOptionPane.showInputDialog(null, "enter number of item"));
        double cost = Double.parseDouble(JOptionPane.showInputDialog(null, "enter cost"));
        double costToatl = call.getCost(number, cost);
        System.out.println(costToatl);

        //calling private method
        //it is impossible to call a private method in another class

        //calling a public method
        call.noArguments();

        //calling a protected method, passed by value
        int product = Integer.parseInt(JOptionPane.showInputDialog(null, "enter number of item"));
        double costs = Double.parseDouble(JOptionPane.showInputDialog(null, "enter cost"));
        double totalPrice = call.protectedMethod(product,costs);
        System.out.println(totalPrice);
    }
}
