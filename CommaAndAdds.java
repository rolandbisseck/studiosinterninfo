import javax.swing.*;


/**
 * Created by Packard bell on 9/21/2015.
 */
public class CommaAndAdds
{
    public static void main(String[] args)
    {

            String[] inputWord = {"[]","[\"ABC\"]","[\"ABC\", \"DEF\"]","[\"ABC\", \"DEF\", \"G\", \"H\"]"};
            String output = "";
            // Scanner input = new Scanner(System.in);
            //System.out.println("");
            int lastComma = 0;

            for(int i = 0; i < inputWord.length; i++) {

                lastComma = getLastComma(inputWord[i]);


                for(int x = 0; x < inputWord[i].length(); x++){

                    char aChar = inputWord[i].charAt(x);
                    if(aChar == '[')
                        output += '{';
                    else if(aChar == ']')
                        output += '}';
                    else if(aChar == '"')
                        output += "";
                    else if(x == lastComma)
                        output += " and ";
                    else
                        output += aChar ;
                }

                output += "\n";
                System.out.println(inputWord[i]);

            }
            System.out.println("\n" + output);

        }


        public static int getLastComma(String item){
            int number = 0;
            for(int i = 0; i < item.length(); i++){
                if(item.charAt(i) == ',')
                    number = i;
            }
            return number;
        }

    }