import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Packard bell on 10/9/2015.
 */
public class ComputerRockPaperScissor
{
    private String name;

    //a constructor that takes one parameter
    public ComputerRockPaperScissor(String name)
    {
        this.name = name;
    }
    //a get method that return the name of the computer
    public String getName()
    {
        return this.name;
    }
    //a method that will generate the choice made by the computer and return the the choice made by the computer
    public String generateChoice()
    {
        String computerChoice = "";

        String[] choice = {"rock", "paper", "scissors"};

        Random random = new Random();
        //generate random number
        int randomNumber = random.nextInt(3);
        //determine the generated number and assign it to the corresponding index of the array string of choice
        if(randomNumber == 0)
        {
            computerChoice = choice[0];
        }
        else if(randomNumber == 1)
        {
            computerChoice = choice[1];
        }
        else
        {
            computerChoice = choice[2];
        }
        //return the computer choice
         return computerChoice;
    }

}
