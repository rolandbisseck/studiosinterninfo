import java.util.ArrayList;

/**
 * Created by Packard bell on 9/23/2015.
 */
import java.text.DecimalFormat;
public class DollarSeven
{
        public static void main(String[] args)
        {
        for (double four = 1.00; four < 7.12; four += 0.01)
        {
            four = roundTwoDecimals(four);
            for (double three = 1.00; three < 7.12; three += 0.01)
            {
                three = roundTwoDecimals(three);
                for (double two = 1.00; two < 7.12; two += 0.01)
                {
                    two = roundTwoDecimals(two);
                    double one = 0.0;
                    for (one = 0.00; one  < 7.12 ;one += 0.01)
                    {
                        if (roundTwoDecimals(one + two + three + four) == 7.11 && roundTwoDecimals(one * two * three * four) == 7.11)
                        {
                            System.out.println(one + "," + two + "," + three + "," + four);
                            System.exit(0);
                        }
                    }

                }

                }
            }
        }
    private static double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }
}

