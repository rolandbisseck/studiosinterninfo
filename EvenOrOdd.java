import javax.swing.*;

/**
 * Created by Packard bell on 9/17/2015.
 */
public class EvenOrOdd
{
    public static void main(String[] args)
    {
        int number = Integer.parseInt(JOptionPane.showInputDialog(null, "enter a number"));
        String result ="";

         if(number % 3 == 0 && number % 2 == 0)
        {
        result = number + "is an odd number and even";
        }
        else if(number % 2 == 0)
        {
            result = number + "is an even number";
        }
        else if(number % 3 == 0)
        {
            result = number + "is an odd number";
        }
        else
         {
             result = number + "is neither an even or odd number";
         }

        System.out.println(result);
    }
}
