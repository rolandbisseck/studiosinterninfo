import javax.swing.*;
import java.util.*;

/**
 * Created by Packard bell on 9/21/2015.
 */
public class GoldbachConjecturePrimeNumber
{
    public static void main(String[] args)
    {
        int sum = Integer.parseInt(JOptionPane.showInputDialog(null, "ENTER A NUMBER"));
       String theTwoPrime  = findTheTwoPrime(sum);
        //this output will print the two prime number thich added together give the even number the user enters
        System.out.print(theTwoPrime);

        System.out.println();

        ArrayList<Integer> listOfPrimes = new ArrayList<Integer>();
        listOfPrimes = listOfPrime(sum);
        //this output will print the list of prime in the range of the even number the user enters
        System.out.print(listOfPrimes);

    }
    //in this function we determine if a number is a prime number or not
    public static boolean isPrime(int number)
    {
        boolean isPrime = true;
        for(int x = 2; x <= number / 2; x++)
        {
            if(number % x == 0)
            {
                isPrime = false;
            }

        }
        return isPrime;
    }
    // in this function we determine the number of prime number in a certain range
    public static ArrayList<Integer> listOfPrime(int number)
    {
        ArrayList<Integer> listOfPrime = new ArrayList<  Integer>();
        for(int x = 2; x < number + 1; x++)
        {
            if(isPrime(x))
            {
                listOfPrime.add(x);
            }
        }
        return listOfPrime;
    }
    //this method finds the 2 prime numbers that add to a given even number greater number than 2
    public static String findTheTwoPrime(int sum)
    {
        int result = 0;
        String thePrimes = "";

        ArrayList<Integer> listPrime = new ArrayList<Integer>();

            listPrime = listOfPrime(sum);
            for(int x= 0; x < listPrime.size(); x++)
            {
                for(int y =0; y < listPrime.size(); y++)
                {
                    result = listPrime.get(x) + listPrime.get(y);

                    if(result == sum)
                    {
                        /*after additioning prime numbers, when there is a match with the sum given by the user, it stop immediatly,
                        that is why i used the break key*/
                        thePrimes = listPrime.get(x) + "," + listPrime.get(y);
                        break;

                    }


                }

            }




        return thePrimes;

    }
}
