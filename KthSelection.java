/**
 * Created by Packard bell on 9/23/2015.
 */
public class KthSelection
{
    public static int[] doSelectionSort(int[] kElements){

        for (int x = 0; x < kElements.length - 1; x++)
        {
            int index = x;
            for (int j = x + 1; j < kElements.length; j++)
                if (kElements[j] < kElements[index])
                    index = j;

            int smallerNumber = kElements[index];
            kElements[index] = kElements[x];
            kElements[x] = smallerNumber;
        }
        return kElements;
    }

    public static void main(String array[]){

        int[] Elements = {10,32,5,51,7,62,90,2,15};
        int[] selectionArray = doSelectionSort(Elements);
        for(int x = 0;x < selectionArray.length; x++){

            System.out.print(selectionArray[x] + " ");
        }
    }

}
