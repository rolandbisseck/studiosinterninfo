/**
 * Created by Packard bell on 9/21/2015.
 */
public class MultiplicationTable
{
    public static void main(String[] args)
    {
        /*i used two for loop,
        the first one is the one for multiples from 1 to 12
        and the second one for multiplying. they from 1 to 12 as well
        */
        for (int x = 1; x <= 12; x++)
        {
            for (int y = 1; y <= 12; y++)
            {
                System.out.print(x + "x" + y + "=" + (x * y) + "\n");
            }
            System.out.println();
        }
    }

}
