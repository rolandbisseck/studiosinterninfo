import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Packard bell on 9/22/2015.
 */
public class PancakeFlipping
{
    public static void main(String[] args)
    {
        int numbers[] = {12,-4,-3,-2,0,2,5,7,9};

        pancakeFlipping(numbers);
        for (int z = 0; z < numbers.length - 1; z++)
        {
            System.out.print(numbers[z] + " ");
        }

        System.out.print(numbers[numbers.length - 1]);
    }
    //this method sort numbers using the pancake flipping algorithm
    public static void pancakeFlipping(int[] numbers)
    {
        for (int x = 0; x < numbers.length; x++)
        {
            for (int y = x + 1; y < numbers.length; y++)
            {
                if (numbers[x] > numbers[y]) {
                    int temp = numbers[x];
                    numbers[x] = numbers[y];
                    numbers[y] = temp;
                }
            }
        }

    }


}
