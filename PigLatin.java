import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.io.*;

/**
 * Created by Packard bell on 9/22/2015.
 */
public class PigLatin
{

    public static void main (String[] arguments)
    {
        String myWord = JOptionPane.showInputDialog(null, "enter a english word");
        String word = pigLatin(myWord);
        System.out.println(word);

        String piglatin = JOptionPane.showInputDialog(null, "enter a pig latin word");
        String english = english(piglatin);
        System.out.println(english);
    }
    public static boolean isVowel(Character vowel)
    {
        boolean isValid = false;
        switch (vowel)
        {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':

                isValid = true;
        }
        return isValid;
    }

    public static String pigLatin(String word)
    {
        String output = "";

        if(isVowel(word.charAt(0)))
        {
            word = word + "-way";
        }
        else
        {
            word = word + "-";

            for(int x = 1; x < word.length() -1; x++)
            {
                if(!isVowel(word.charAt(0)))
                {
                    word = word.substring(1,word.length()) + word.charAt(0);
                }
            }
            output = "ay";
        }
        return word + output;
    }
    public static String english(String word)
    {
        String myWords1 = "";
        String reverse = "";
        String[] element = word.split("-");
        String element1 = element[0];
        String element2 = element[1];
        if(isVowel(word.charAt(0)))
        {
          reverse = element1;
        }
       if(!isVowel(element2.charAt(0)) && element2.charAt(0) != 'w')
        {
            String[] myWords = element2.split("a");
            myWords1 = myWords[0];

            reverse = myWords1 + element1;
        }

        return reverse;
    }



}
