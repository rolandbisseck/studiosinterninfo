import java.util.ArrayList;

/**
 * Created by Packard bell on 10/9/2015.
 */
public class PlayerRockPaperScissor
{
    private String name;
    private String playerChoice;

    //a constructor which takes 2 parameter the name and the choice
    public PlayerRockPaperScissor(String name, String playerChoice)
    {
        this.name = name;
        this.playerChoice = playerChoice;
    }
    //a get method that return the name
    public String getName()
    {
        return this.name;
    }
    //a get method that return the choice made by the player
    public String getChoice()
    {
        return this.playerChoice;
    }


}
