/**
 * Created by Packard bell on 10/6/2015.
 */
import java.util.*;
public class PriorityQueues implements Comparable<PriorityQueues>
{

    final int priority;
    final String name;

    public PriorityQueues(int p, String n) {
        priority = p;
        name = n;
    }

    public String toString() {
        return priority + ", " + name;
    }

    public int compareTo(PriorityQueues other) {
        return priority < other.priority ? -1 : priority > other.priority ? 1 : 0;
    }

    public static final void main(String[] args) {
        PriorityQueue<PriorityQueues> pq = new PriorityQueue<PriorityQueues>();
        pq.add(new PriorityQueues(3, "with its colleagues"));
        pq.add(new PriorityQueues(4, "the internship"));
        pq.add(new PriorityQueues(5, "enjoy"));
        pq.add(new PriorityQueues(1, "infoware studios"));
        pq.add(new PriorityQueues(2, "roland bisseck bisseck"));

        while (!pq.isEmpty())
            System.out.println(pq.remove());
    }

}
