/**
 * Created by Packard bell on 9/22/2015.
 */
import com.sun.deploy.util.ArrayUtil;

import java.util.ArrayList;
public class QuickSort
{
    public static void main(String[] args)
    {
        int[] list = {2,3,2,5,6,1,15, 30,3,14,12};
        quickSort(list);
        for(int x = 0; x < list.length; x++)
        {
            System.out.print(list[x] + " ");
        }

    }
    public static void quickSort(int[] list)
    {
        quickSort(list,0,list.length -1);
    }
    public static void quickSort(int[] list, int first, int last)
    {
        if(last > first)
        {
            int pivotIndex = separation(list,first,last);
            quickSort(list,first,pivotIndex -1);
        }
    }
    public static int separation(int[] list, int first, int last)
    {
        int pivot = list[first];//choose the first element as the pivot
        int less = first + 1;
        int greater = last;
        while(greater > less)
        {
            //i search forward from left
            while(less <= greater && list[less] <= pivot)
            {
                less++;
            }
            //i search left from right
            while(less <= greater && list[greater] > pivot)
            {
                greater--;
            }
            //i swap any two element in the list
            if(greater > less)
            {
                int temp = list[greater];
                list[greater] = list[less];
                list[less] = temp;
            }
        }
        while(greater > first && list[greater] >= pivot)
        {
           greater--;
        }
        //i swap pivot with list[greater]
        if(pivot > list[greater])
        {
            list[first] = list[greater];
            list[greater] = pivot;

            return greater;
        }
        else
        {
            return first;
        }

    }
}
