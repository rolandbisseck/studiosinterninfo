/**
 * Created by Packard bell on 9/23/2015.
 */
public class RBGNumbers
{
    /*
    Format an RGB value (three 1-byte numbers) as a 6-digit hexadecimal string.
    For example if R = 128, G = 192, and B = 79, then output #80C04F.
     */
    public static void main(String[] args)
    {
        String format = formatRGB(128,192,72);
        System.out.println(format);
    }
    public static String formatRGB ( int r, int g, int b ) {
        return String.format ( "%02X%02X%02X", r, g, b );
    }
}
