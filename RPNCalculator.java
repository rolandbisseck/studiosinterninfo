/**
 * Created by Packard bell on 9/23/2015.
 */
import javax.swing.*;
import java.util.*;
public class RPNCalculator
{

    public static void main(String[] args)
    {
        String expression = JOptionPane.showInputDialog(null, "enter the expresionn");

        String myExpression = cleanExpr(expression);

        LinkedList<Double> stack = new LinkedList<Double>();

        System.out.println("Input\tOperation\tStack after");
        for(String token:myExpression.split(" "))
        {
            System.out.print(token+"\t");
            Double tokenNum = null;
            try
            {
                tokenNum = Double.parseDouble(token);
            }catch(NumberFormatException e)
            {
                e.getMessage();
            }
            // here we push and save numbers that are not null
            if(tokenNum != null)
            {
                System.out.print("Push\t\t");
                stack.push(Double.parseDouble(token+""));
                //in this else if part we check for the symbol of the operant
            }else if(token.equals("*"))
            {
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(firstOperand * secondOperand);
            }else if(token.equals("/"))
            {
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(firstOperand / secondOperand);
            }else if(token.equals("-"))
            {
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(firstOperand - secondOperand);
            }else if(token.equals("+"))
            {
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(firstOperand + secondOperand);
            }else if(token.equals("^"))
            {
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(Math.pow(firstOperand, secondOperand));
            }else
            {//just in case nothing was found
                System.out.println("Error");
                return;
            }
            System.out.println(stack);
        }
        System.out.println("Final answer: " + stack.pop());
    }

    private static String cleanExpr(String expr){
        //remove all non-operators, non-whitespace, and non digit chars
        return expr.replaceAll("[^\\^\\*\\+\\-\\d/\\s]", "");
    }

}
