import javax.swing.*;

/**
 * Created by Packard bell on 10/1/2015.
 */
public class RabinKarpStringSearch
{
    public static void main (String[] args)
    {
        String text = JOptionPane.showInputDialog(null, "enter a text");
        String pattern = JOptionPane.showInputDialog(null, "enter the pattern");

        //calling method and demonstrate the outcome
        int hash = rabinKarp(text, pattern);
        System.out.println(hash);
    }
    /*this method will hash the character of the string to numbers.
        it more like retrieving the number if the index instead of the characters themselve
     */
    public  static int hashString(String word, int length)
    {
        int hash = 0;
        for(int x = 0; x < length; x++)
        {
            hash = (int)word.charAt(x);
        }
        return hash;
    }
    /*this method compare the hash number of the text to the hash number of the pattern
     if there is a match the method should return the first index from where the match start to confirm the match,
     else it would return a 0 to say there was not a match found
     */
    public static int rabinKarp(String text, String pattern)
    {
        int index = 0;
        int textHash = 0;
        int patternHash = 0;

        textHash = hashString(text.substring(1,text.length()),pattern.length());
        patternHash = hashString(pattern, pattern.length());

        for(int x = 0; x < text.length() - pattern.length() + 1; x++)
        {
            if(textHash == patternHash)
            {
                index = x;
            }

            textHash = hashString(text.substring(x,text.length()),pattern.length());
        }
        return index;
    }
}
