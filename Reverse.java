import javax.swing.*;

/**
 * Created by Packard bell on 9/22/2015.
 */
public class Reverse
{
    /*Write a program to reverse a string and reverse an array.  Do not use the built in functions of your language*/
    public  static void main (String[] args)
    {
        String[] ar = {"roland "," alain ","bisseck"};
        String reverseArray= reverseArray(ar);
       System.out.println(reverseArray);

        String word = JOptionPane.showInputDialog(null, "enter a word");
        String reverseString = reverse(word);
        System.out.println(reverseString);
    }
    //this method reverse a string
    public static String reverse(String word)
    {
        String reverse = "";
        for(int x = word.length() - 1; x >= 0; x--)
        {
            reverse = reverse + word.charAt(x);
        }
        return reverse;
    }
    //this method reverse an array and also reverse each word belonging to the array
    public static String reverseArray(String[] word)
    {
        String reverse = "";
        String re = "";

        //this for loop is used to reverse the array
        for(int x = word.length - 1; x >= 0; --x)
        {
            reverse = reverse + word[x];
            //this for loop is used to reverse each word of the array
            for(int y = word[x].length() - 1; y >= 0; y--)
            {
                re = re + word[x].charAt(y);
            }
        }
        return reverse + " :" + re;
    }
}
