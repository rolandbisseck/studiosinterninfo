
/**
 * Created by Packard bell on 9/23/2015.
 */
import java.util.*;
public class RingOfDeath
{
    public static void main(String[] args)
    {
        System.out.println("josephus survived in position: " + execute(41, 3));
    }
    public static int execute(int n, int m)
    {
        int killIdx = 0;
        ArrayList<Integer> prisoners = new ArrayList<Integer>(n);
        for(int i = 0;i < n;i++)
        {
            prisoners.add(i);
        }
        System.out.println("order in which prisoners are excecuted:");
        while(prisoners.size() > 1)
        {
            killIdx = (killIdx + m - 1) % prisoners.size();
            System.out.print(prisoners.get(killIdx) + " ");
            prisoners.remove(killIdx);
        }
        System.out.println();
        return prisoners.get(0);
    }

}