import java.util.ArrayList;

/**
 * Created by Packard bell on 10/9/2015.
 */
public class RockPaperScissor
{
    private PlayerRockPaperScissor player;
    private  ComputerRockPaperScissor computer;

    //a constructor that accept two parameter, player of type PlayerRockPaperScissor and computer of type ComputerRockPaperScissor
    public RockPaperScissor(PlayerRockPaperScissor player, ComputerRockPaperScissor computer)
    {
        //initialise
        this.player = player;
        this.computer = computer;
    }
    //method that return a player
    public PlayerRockPaperScissor getPlayer()
    {
        return this.player;
    }
    //method that return a computer
    public ComputerRockPaperScissor getComputer()
    {
        return this.computer;
    }

    //this method will determine the winner according to the rules
    public String determineWinner()
    {
        String winner  = "";

        String playerChoice = "";
        String computerChoice = "";


        playerChoice = player.getChoice();
        computerChoice = computer.generateChoice();

        if(playerChoice.equals("rock") && computerChoice.equals("scissors"))
        {
            winner = player.getName();
        }
        else if(playerChoice.equals("scissors") && computerChoice.equals("paper"))
        {
            winner = player.getName();
        }
        else if(playerChoice.equals("paper") && computerChoice.equals("rock"))
        {
            winner = player.getName();
        }
        else if(playerChoice.equals("rock") && computerChoice.equals("paper"))
        {
            winner = computer.getName();
        }
        else if(playerChoice.equals("paper") && computerChoice.equals("scissors"))
        {
            winner = computer.getName();
        }
        else if(playerChoice.equals("scissors") && computerChoice.equals("rock"))
        {
            winner = computer.getName();
        }
        else if(playerChoice.equals("rock") && computerChoice.equals("rock"))
        {
            winner = "no onw won";
        }
        else if(playerChoice.equals("paper") && computerChoice.equals("paper"))
        {
            winner = "no one won";
        }
        else if(playerChoice.equals("scissors") && computerChoice.equals("scissors"))
        {
            winner = "no one won";
        }
        else
        {
            winner = "you did not play";
        }

        return winner;
    }


}
