import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Packard bell on 10/9/2015.
 */
public class RockPaperScissorApplication
{
    public static void main (String[] args)
    {
        //i prompt the user to enter the name of the player, the name of the computer and the player's choice
        String playerName = JOptionPane.showInputDialog(null, "enter the name of the player");
        String computerName = JOptionPane.showInputDialog(null, "enter something to identify the computer(name)");
        String playerChoice = JOptionPane.showInputDialog(null, "enter your choice as player (Rock, paper or Scissor)");

        //i instantiate object of type PPlayerRockPaperScissor,ComputerRockPaperScissor and  RockPaperScissor
        PlayerRockPaperScissor player = new PlayerRockPaperScissor(playerName,playerChoice);
        ComputerRockPaperScissor computer = new ComputerRockPaperScissor(computerName);
        RockPaperScissor application = new RockPaperScissor(player,computer);

        // calling methods

        String playerChoices = player.getChoice();
        String computerChoice = computer.generateChoice();
        String determineWinner = application.determineWinner();

        //displaying the outputs
        System.out.print("this is the choice the player " +application.getPlayer().getName() + " has made: " + playerChoices + "\n");
        System.out.print("this is the generated choice for the computer is: " + " " + computerChoice);
        System.out.print("\n");
        System.out.print("and the winner according to the rules is: " + determineWinner );



    }
}
