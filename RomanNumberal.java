import javafx.scene.chart.PieChart;

/**
 * Created by Packard bell on 9/22/2015.
 */
import javax.swing.*;
import java.util.*;
public class RomanNumberal
{
    public static void main(String[] args)
    {
        String roman1 = JOptionPane.showInputDialog(null, "Enter a Roman Notation");
        String roman2 = JOptionPane.showInputDialog(null, "Enter a Roman Notation");
        System.out.print(roman1 + " + " + roman2);
        int add = addRoman(roman1, roman2);
        System.out.println( " = " + add);

    }
    //in this method we assign each roman symbol to its correspondant number using parralel array
    public static int determineRoman(String romanNumeral)
    {
        int sum = 0;
        //declare parralel array
        char[] romanNotation = {'I','V','X','L','C','D','M'};
        int[] numeral = {1,5,10,50,100,500,1000};

        for(int x = 0; x < romanNotation.length; x++)
        {
            for(int y = 0; y < romanNumeral.length(); y++)
            {
                if(romanNotation[x] == romanNumeral.charAt(y))
                {
                    sum = sum + numeral[x];
                }
            }
        }

        return sum;
    }
    //this method add two roman numerals
    public static int addRoman(String romanNumeral1, String romanNumeral2)
    {
        int add = 0;
        //calling the previous method to perfornm
        int sum1 = determineRoman(romanNumeral1);
        int sum2 = determineRoman(romanNumeral2);

        add = sum1 + sum2;
        return add;
    }



}
