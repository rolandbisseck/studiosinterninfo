import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Packard bell on 9/23/2015.
 */
public class SiegeOfEratosthenes
{
    public static void main (String[] args)
    {
        int number = Integer.parseInt(JOptionPane.showInputDialog(null, "enter a number"));
        boolean[] isPrime = new boolean[number];
        isPrime[0] = false;
        for(int x = 1; x < number; x++)
        {
            isPrime[x] = true;
        }
        for(int y = 2; y <=number; y++)
        {

            if(isPrime[y -1])
            {
                //this will print all the prime number
                System.out.print(y + " ");

                //this loop will cross off all the subsequent multiple of y
                for(int z = y * y; z <=number; z = z + y)
                {
                    isPrime[z -1] = false;
                }
            }
        }
    }
}
