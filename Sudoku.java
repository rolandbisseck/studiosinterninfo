import javax.swing.*;
import java.util.Scanner;

/**
 * Created by Packard bell on 9/24/2015.
 */
public class Sudoku
{
    public static void main(String[] args)
    {
        int[][] grid = readSolution();
        if(isSudokuValid(grid))
        {
            System.out.println("you made it, congrat");
        }
        else
        {
            System.out.println("no worries, next time you may win");
        }

    }
    // in this method i read the sudoku game from the console
    public static int[][] readSolution()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("play:");
        int[][] grid = new int[9][9];
        for(int x = 0; x < 9; x++)
        {
            for(int y = 0; y < 9; y++)
            {
                grid[x][y] = input.nextInt();

            }
        }
        return grid;
    }
    //i check whether a solution is valid
    public static boolean isSudokuValid(int[][] grid)
    {
        for(int x =0 ; x < 9; x++)
        {
            for(int y = 0; y < 9; y++)
            {
                if(grid[x][y] < 1 || grid[x][y] > 9 || !isGridValid(x,y,grid))
                 {
                    return  false;
                 }
            }
        }
        return  true;
    }
    //in this method i check whether the grid[x][y] is valid in the grid
    public static boolean isGridValid(int x, int y, int[][] grid)
    {
        // i check if th grid is valid in its x row
        for(int column = 0; column < 9; column++)
        {
            if(column != y && grid[x][column] == grid[x][y])
            {
                return false;
            }

        }
        //check if the grid is valid in its y column
        for(int row = 0; row < 9; row++)
        {
            if(row != x && grid[row][y] == grid[x][y])
            {
                return false;
            }
        }
        for(int row = (x /3) * 3; row < (x / 3) * 3 + 3; row++)
        {
            for(int column = (y / 3) * 3; column < (y / 3) * 3 + 3; column++)
            {
                return  false;
            }
        }
        return  true;
    }
}
