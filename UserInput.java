
import java.util.Scanner;

/**
 * Created by Packard bell on 9/22/2015.
 */
public class UserInput
{
    /*
    * In this task, the goal is to input a string and the integer 75000, from the text console*/
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        String name =  scan.next();

        int number = scan.nextInt();

        System.out.println(name +","+ number);
    }
}
