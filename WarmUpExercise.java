import java.util.Random;

/**
 * Created by Packard bell on 10/9/2015.
 */

public class WarmUpExercise
{
    public static void main(String[] args)
    {
        Random rand = new Random();
        String[] element = {"Apple", "Pears", "Grapes", "Oranges"};
        double[] weight = {0.25, 0.5, 0.2, 0.05};
        double[] accumulateSum = new double[weight.length];
        double sum = 0;

        double accumulativeSum = 0.0;
        int randNumber = 0;
        String fruit = "";
        double cumulativeWeight = 0;
        System.out.print("the output with the cumulative weight:");
        System.out.print("\n");
        for(int x = 0; x < weight.length; x++)
        {
            sum += weight[x];
            accumulateSum[x] = sum;

            System.out.print(element[x] + " => " + accumulateSum[x] + " ");


        }
        for(int y = 0; y < accumulateSum.length; y++)
        {
            randNumber = rand.nextInt(2);
            if(accumulateSum[y] > randNumber)
            {

                fruit = element[y];
                cumulativeWeight = accumulateSum[y];

            }

        }
        System.out.print("\n"+ "the cumulative element that is greater than the random number generated is:");
        System.out.print("\n" + cumulativeWeight + " " + fruit);

    }
}
